"use strict";

module.exports = (function () {
  return Object.create || function (proto) {
    function Type() {}
    Type.prototype = proto;
    return new Type();
  };
})();
