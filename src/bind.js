"use strict";

import bind from 'function-bind';
import uncurryThis from './uncurry';

module.exports = uncurryThis(bind);
