"use strict";

import setPrototypeOf from 'setprototypeof';
import getPrototypeOf from 'get-prototype-of';
import enumerateProto from './enumerate';
import bind from './bind';
import reduce from 'array-reduce';
import reduceOwn from './reduce-own';
import create from './create';
import assign from './own-assign';
import isFunction from 'is-function';
import { getter, setter } from './transform-property';
import getOwnPropertyDescriptor from './get-own-prop';
import createNull from './create-null';

const bindReducer = (o) => {
  return (r, key, link) => {
    const descriptor = getOwnPropertyDescriptor(link, key);
    if (descriptor.get || descriptor.set) {
      if (descriptor.get) r[getter(key)] = bind(descriptor.get, o);
      if (descriptor.set) r[setter(key)] = bind(descriptor.set, o);
      return r;
    }
    const { value } = descriptor;
    if (isFunction(value)) 
      r[key] = bind(value, o);
    return r;
  };
};

const singular = (o) => (
  setPrototypeOf(o, reduce(enumerateProto(o).reverse(), (r, v) => (
    assign(create(r), reduceOwn(v, bindReducer(o), createNull()))
  ), getPrototypeOf(o)))
);

singular.from = (o) => (
  reduce([ reduceOwn(o, bindReducer(o), createNull()) ].concat(enumerateProto(o)).reverse(), (r, v) => (
    assign(create(r), reduceOwn(v, bindReducer(o), createNull()))
  ), null)
);

module.exports = singular;
