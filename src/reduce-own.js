import reduce from 'array-reduce';
import getOwnPropertyNames from './own';

module.exports = (o, cb, start, thisArg) => (
  reduce(getOwnPropertyNames(o), (r, v) => cb.call(thisArg, r, v, o), start)
);
