"use strict";

module.exports = {
  getter: (prop) => ('get' + prop.substr(0, 1).toUpperCase() + prop.substr(1)),
  setter: (prop) => ('set' + prop.substr(0, 1).toUpperCase() + prop.substr(1))
};
