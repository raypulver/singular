"use strict";

import getOwnPropertyNames from './own';
import forEach from './for-each';
import getOwnPropertyDescriptor from './get-own-prop';
import { defineProperty } from 'object-define-property';

module.exports = (dest, src) => {
  forEach(getOwnPropertyNames(src), (v) => {
    defineProperty(dest, v, getOwnPropertyDescriptor(src, v));
  });
  return dest;
};
