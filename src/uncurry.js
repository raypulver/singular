"use strict";

module.exports = (fn) => {
  /* jshint ignore:start */
  return function (...args) {
    return fn.apply(args[0], args.slice(1));
  };
  /* jshint ignore:end */
};
