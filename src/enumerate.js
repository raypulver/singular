"use strict";

import getPrototypeOf from 'get-prototype-of';
import assign from './own-assign';
import createNull from './create-null';

module.exports = (o) => {
  let retval = [];
  while ((o = getPrototypeOf(o))) {
    retval.push(assign(createNull(), o));
  }
  return retval;
};
