"use strict";

import reduce from 'array-reduce';
import getPrototypeOf from 'get-prototype-of';
import isUndefined from 'is-undefined';
import create from './create';
import getOwnPropertyNames from './own';

module.exports = (o, cb, thisArg) => (reduce(getOwnPropertyNames(o), (r, v) => {
  const mapped = cb.call(thisArg, o[v], v, o);
  if (!isUndefined(mapped)) r[v] = mapped;
  return r;
}, create(getPrototypeOf(o))));
