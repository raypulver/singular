"use strict";

const { getOwnPropertyNames, prototype: ObjectProto } = Object;
const { hasOwnProperty } = ObjectProto;

module.exports = getOwnPropertyNames || function (o) {
  const names = [];
  for (let k in o) {
    if (hasOwnProperty.call(o, k)) names.push(k);
  }
  return names;
};
