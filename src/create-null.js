"use strict";

import bind from './bind';
import create from './create';

module.exports = bind(create, null, null);
