"use strict";

import uncurryThis from './uncurry';

const { forEach } = Array.prototype;

module.exports = forEach && uncurryThis(forEach) || function (arr, cb, thisArg) {
  for (let i = 0; i < arr.length; ++i) {
    cb.call(thisArg, arr[i], i, arr);
  }
};
