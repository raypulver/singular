"use strict";

import uncurryThis from './uncurry';

const { getOwnPropertyDescriptor, prototype: ObjectProto } = Object;
const { hasOwnProperty: hasOwnPropertyNative } = ObjectProto;

const hasOwnProperty = uncurryThis(hasOwnPropertyNative);

module.exports = (function () {
  return getOwnPropertyDescriptor || function (o, prop) {
    return hasOwnProperty(o, prop) && { value: o[prop], configurable: true, writable: true, enumerable: true } || void 0;
  };
})();
