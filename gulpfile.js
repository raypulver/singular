"use strict";

var join = require('path').join,
    sep = require('os').sep,
    writeFileSync = require('fs').writeFileSync,
    gulp = require('gulp'),
    gutil = require('gulp-util'),
    jshint = require('gulp-jshint'),
    webpack = require('webpack'),
    webpackConfig = require('./webpack.config'),
    webpackConfigDev = webpackConfig.dev,
    webpackConfigMin = webpackConfig.min,
    mocha = require('gulp-mocha'),
    babel = require('gulp-babel');

var jshintConfig = {};
require.extensions['.json'](jshintConfig, './.jshintrc');
jshintConfig = jshintConfig.exports;

var keys = Object.keys,
    isArray = Array.isArray,
    task = gulp.task.bind(gulp),
    tasks = gulp.tasks;

var packageJson = require(join(__dirname, 'package'));

function prettyStringify(v) {
  return JSON.stringify(v, null, 1);
}

function forOwn(o, cb) {
  keys(o).forEach(function (v) {
    cb(o[v], v, o);
  });
}

function binPath(exe) {
  return join('node_modules', '.bin', exe);
}

function nodeTask(exe) {
  return 'node ' + binPath(exe);
}

function nodeTaskArgs(exe, args) {
  if (!isArray(args)) args = [args];
  return nodeTask(exe) + ' ' + args.join(' ');
}

var gulpTask = nodeTaskArgs.bind(null, 'gulp');

function appendSep(str) {
  return String(str) + sep;
}

function doWebpack(cfg, next) {
  webpack(cfg, function (err, stats) {
    if (err) throw new gutil.PluginError('webpack', err);
    gutil.log('[webpack]', stats.toString({
      colors: true,
      chunkModules: true
    }));
    next();
  });
}

task('build:tasks', function () {
  packageJson.scripts = {};
  forOwn(tasks, function (v, k) {
    packageJson.scripts[k] = gulpTask(k);
  });
  writeFileSync('package.json', prettyStringify(packageJson));
});

task('build', ['webpack:dev', 'webpack:min']);

task('webpack:dev', function (next) {
  doWebpack(webpackConfigDev, next);
});

task('webpack:min', function (next) {
  doWebpack(webpackConfigMin, next);
});

task('jshint', function () {
  return gulp.src('./src/**/*.js')
    .pipe(jshint(jshintConfig))
    .pipe(jshint.reporter('jshint-stylish'));
});

task('test', function () {
  return gulp.src('test/test.js', { read: false })
    .pipe(mocha({ reporter: 'nyan' }));
});
