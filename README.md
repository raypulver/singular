# singular

Simple module that exports a function that accepts an object, array, or even a function as an argument, and injects a prototype chain of all the functions in the original chain bound to the instance. Works within NodeJS and any browser that supports `__proto__`.

If `__proto__` is unavailable, you do not wish to alter the prototype of the instance, or you need an object which also includes bound copies of the instance's functions assigned as ownProps, use `singular.from` which returns a new object containing the bound functions.

## Example

```js

var singular = require('singular');
var array = [1];
singular(array);
var push = array.push;
push(2);
console.log(array);
// [1, 2]

function Type() {
  this.increment = function () { this.val++; };
  this.val = 0;
}

Type.prototype = {
  decrement: function () { this.val--; },
  getValue: function () { return this.val; }
};

var instance = new Type(),
    methods = singular.from(instance),
    increment = methods.increment,
    decrement = methods.decrement,
    getValue = methods.getValue;

increment();
increment();
decrement();

getValue();

// 1

## Author

Raymond Pulver
