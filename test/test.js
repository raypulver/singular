var singular = require('..');
var expect = require('chai').expect;

describe('singular default fn', function () {
  it('should bind functions to the prototype', function () {
    var array = [1];
    singular(array);
    var push = array.push;
    push(2);
    expect(array.length).to.equal(2);
    expect(array[1]).to.equal(2);
  });
});

describe('singular.from', function () {
  it('should create a new object of bound functions from prototype and ownProps', function () {
    function Type() {
      this.increment = function () { this.val++; };
      this.val = 0;
    }

    Type.prototype = {
      decrement: function () { this.val--; },
      getValue: function () { return this.val; }
    };

    var instance = new Type(),
        methods = singular.from(instance),
        increment = methods.increment,
        decrement = methods.decrement,
        getValue = methods.getValue;

    increment();
    increment();
    decrement();

    expect(getValue()).to.equal(1);
    expect(Object.keys(methods).length).to.equal(1);

  });
});
