(function webpackUniversalModuleDefinition(root, factory) {
	if(typeof exports === 'object' && typeof module === 'object')
		module.exports = factory();
	else if(typeof define === 'function' && define.amd)
		define([], factory);
	else if(typeof exports === 'object')
		exports["singular"] = factory();
	else
		root["singular"] = factory();
})(this, function() {
return /******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId])
/******/ 			return installedModules[moduleId].exports;
/******/
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			exports: {},
/******/ 			id: moduleId,
/******/ 			loaded: false
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.loaded = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(0);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ function(module, exports, __webpack_require__) {

	"use strict";
	
	var _setprototypeof = __webpack_require__(1);
	
	var _setprototypeof2 = _interopRequireDefault(_setprototypeof);
	
	var _getPrototypeOf = __webpack_require__(2);
	
	var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);
	
	var _enumerate = __webpack_require__(3);
	
	var _enumerate2 = _interopRequireDefault(_enumerate);
	
	var _bind = __webpack_require__(14);
	
	var _bind2 = _interopRequireDefault(_bind);
	
	var _arrayReduce = __webpack_require__(18);
	
	var _arrayReduce2 = _interopRequireDefault(_arrayReduce);
	
	var _reduceOwn = __webpack_require__(19);
	
	var _reduceOwn2 = _interopRequireDefault(_reduceOwn);
	
	var _create = __webpack_require__(17);
	
	var _create2 = _interopRequireDefault(_create);
	
	var _ownAssign = __webpack_require__(4);
	
	var _ownAssign2 = _interopRequireDefault(_ownAssign);
	
	var _isFunction = __webpack_require__(20);
	
	var _isFunction2 = _interopRequireDefault(_isFunction);
	
	var _transformProperty = __webpack_require__(21);
	
	var _getOwnProp = __webpack_require__(8);
	
	var _getOwnProp2 = _interopRequireDefault(_getOwnProp);
	
	var _createNull = __webpack_require__(13);
	
	var _createNull2 = _interopRequireDefault(_createNull);
	
	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }
	
	var bindReducer = function bindReducer(o) {
	  return function (r, key, link) {
	    var descriptor = (0, _getOwnProp2.default)(link, key);
	    if (descriptor.get || descriptor.set) {
	      if (descriptor.get) r[(0, _transformProperty.getter)(key)] = (0, _bind2.default)(descriptor.get, o);
	      if (descriptor.set) r[(0, _transformProperty.setter)(key)] = (0, _bind2.default)(descriptor.set, o);
	      return r;
	    }
	    var value = descriptor.value;
	
	    if ((0, _isFunction2.default)(value)) r[key] = (0, _bind2.default)(value, o);
	    return r;
	  };
	};
	
	var singular = function singular(o) {
	  return (0, _setprototypeof2.default)(o, (0, _arrayReduce2.default)((0, _enumerate2.default)(o).reverse(), function (r, v) {
	    return (0, _ownAssign2.default)((0, _create2.default)(r), (0, _reduceOwn2.default)(v, bindReducer(o), (0, _createNull2.default)()));
	  }, (0, _getPrototypeOf2.default)(o)));
	};
	
	singular.from = function (o) {
	  return (0, _arrayReduce2.default)([(0, _reduceOwn2.default)(o, bindReducer(o), (0, _createNull2.default)())].concat((0, _enumerate2.default)(o)).reverse(), function (r, v) {
	    return (0, _ownAssign2.default)((0, _create2.default)(r), (0, _reduceOwn2.default)(v, bindReducer(o), (0, _createNull2.default)()));
	  }, null);
	};
	
	module.exports = singular;

/***/ },
/* 1 */
/***/ function(module, exports) {

	module.exports = Object.setPrototypeOf || ({__proto__:[]} instanceof Array ? setProtoOf : mixinProperties);
	
	function setProtoOf(obj, proto) {
		obj.__proto__ = proto;
	}
	
	function mixinProperties(obj, proto) {
		for (var prop in proto) {
			obj[prop] = proto[prop];
		}
	}


/***/ },
/* 2 */
/***/ function(module, exports) {

	module.exports = (function() {
		// Use the native lad if he's available
		if ( Object.getPrototypeOf ) {
			return Object.getPrototypeOf;
		}
		else {
			return _getPrototypeOf;
		}
	})();
	
	function _getPrototypeOf( object ) {
	  var proto = object.__proto__;
	  
	  if (proto || proto === null) {
	    return proto;
	  } else if (object.constructor) {
	    return object.constructor.prototype;
	  } else {
	    return Object.prototype;
	  }
	}

/***/ },
/* 3 */
/***/ function(module, exports, __webpack_require__) {

	"use strict";
	
	var _getPrototypeOf = __webpack_require__(2);
	
	var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);
	
	var _ownAssign = __webpack_require__(4);
	
	var _ownAssign2 = _interopRequireDefault(_ownAssign);
	
	var _createNull = __webpack_require__(13);
	
	var _createNull2 = _interopRequireDefault(_createNull);
	
	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }
	
	module.exports = function (o) {
	  var retval = [];
	  while (o = (0, _getPrototypeOf2.default)(o)) {
	    retval.push((0, _ownAssign2.default)((0, _createNull2.default)(), o));
	  }
	  return retval;
	};

/***/ },
/* 4 */
/***/ function(module, exports, __webpack_require__) {

	"use strict";
	
	var _own = __webpack_require__(5);
	
	var _own2 = _interopRequireDefault(_own);
	
	var _forEach = __webpack_require__(6);
	
	var _forEach2 = _interopRequireDefault(_forEach);
	
	var _getOwnProp = __webpack_require__(8);
	
	var _getOwnProp2 = _interopRequireDefault(_getOwnProp);
	
	var _objectDefineProperty = __webpack_require__(9);
	
	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }
	
	module.exports = function (dest, src) {
	  (0, _forEach2.default)((0, _own2.default)(src), function (v) {
	    (0, _objectDefineProperty.defineProperty)(dest, v, (0, _getOwnProp2.default)(src, v));
	  });
	  return dest;
	};

/***/ },
/* 5 */
/***/ function(module, exports) {

	"use strict";
	
	var getOwnPropertyNames = Object.getOwnPropertyNames,
	    ObjectProto = Object.prototype;
	var hasOwnProperty = ObjectProto.hasOwnProperty;
	
	
	module.exports = getOwnPropertyNames || function (o) {
	  var names = [];
	  for (var k in o) {
	    if (hasOwnProperty.call(o, k)) names.push(k);
	  }
	  return names;
	};

/***/ },
/* 6 */
/***/ function(module, exports, __webpack_require__) {

	"use strict";
	
	var _uncurry = __webpack_require__(7);
	
	var _uncurry2 = _interopRequireDefault(_uncurry);
	
	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }
	
	var forEach = Array.prototype.forEach;
	
	
	module.exports = forEach && (0, _uncurry2.default)(forEach) || function (arr, cb, thisArg) {
	  for (var i = 0; i < arr.length; ++i) {
	    cb.call(thisArg, arr[i], i, arr);
	  }
	};

/***/ },
/* 7 */
/***/ function(module, exports) {

	"use strict";
	
	module.exports = function (fn) {
	  /* jshint ignore:start */
	  return function () {
	    for (var _len = arguments.length, args = Array(_len), _key = 0; _key < _len; _key++) {
	      args[_key] = arguments[_key];
	    }
	
	    return fn.apply(args[0], args.slice(1));
	  };
	  /* jshint ignore:end */
	};

/***/ },
/* 8 */
/***/ function(module, exports, __webpack_require__) {

	"use strict";
	
	var _uncurry = __webpack_require__(7);
	
	var _uncurry2 = _interopRequireDefault(_uncurry);
	
	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }
	
	var getOwnPropertyDescriptor = Object.getOwnPropertyDescriptor,
	    ObjectProto = Object.prototype;
	var hasOwnPropertyNative = ObjectProto.hasOwnProperty;
	
	
	var hasOwnProperty = (0, _uncurry2.default)(hasOwnPropertyNative);
	
	module.exports = function () {
	  return getOwnPropertyDescriptor || function (o, prop) {
	    return hasOwnProperty(o, prop) && { value: o[prop], configurable: true, writable: true, enumerable: true } || void 0;
	  };
	}();

/***/ },
/* 9 */
/***/ function(module, exports, __webpack_require__) {

	/* jshint proto:true */
	// ES5 15.2.3.6
	// http://es5.github.com/#x15.2.3.6
	
	// Patch for WebKit and IE8 standard mode
	// Designed by hax <hax.github.com>
	// related issue: https://github.com/es-shims/es5-shim/issues#issue/5
	// IE8 Reference:
	//     http://msdn.microsoft.com/en-us/library/dd282900.aspx
	//     http://msdn.microsoft.com/en-us/library/dd229916.aspx
	// WebKit Bugs:
	//     https://bugs.webkit.org/show_bug.cgi?id=36423
	
	var has = __webpack_require__(10);
	var bind = __webpack_require__(11);
	
	var ERR_NON_OBJECT_DESCRIPTOR = "Property description must be an object: ";
	var ERR_NON_OBJECT_TARGET = "Object.defineProperty called on non-object: ";
	var ERR_ACCESSORS_NOT_SUPPORTED = "getters & setters can not be defined " +
	  "on this javascript engine";
	
	var defProp = Object.defineProperty, defProps = Object.defineProperties;
	var call = Function.prototype.call;
	var prototypeOfObject = Object.prototype;
	
	
	// If JS engine supports accessors creating shortcuts.
	var defineGetter;
	var defineSetter;
	var lookupGetter;
	var lookupSetter;
	var supportsAccessors;
	if ((supportsAccessors = has(prototypeOfObject, "__defineGetter__"))) {
	  defineGetter = bind.call(call, prototypeOfObject.__defineGetter__);
	  defineSetter = bind.call(call, prototypeOfObject.__defineSetter__);
	  lookupGetter = bind.call(call, prototypeOfObject.__lookupGetter__);
	  lookupSetter = bind.call(call, prototypeOfObject.__lookupSetter__);
	}
	
	function doesDefinePropertyWork(object) {
	  try {
	    defProp.call(Object, object, "sentinel", {});
	    return "sentinel" in object;
	  } catch (exception) {
	    // returns falsy
	  }
	}
	
	// check whether defineProperty works if it's given. Otherwise,
	// shim partially.
	if (defProp) {
	  var definePropertyWorksOnObject = doesDefinePropertyWork({});
	  var definePropertyWorksOnDom = typeof document === "undefined" ||
	    doesDefinePropertyWork(document.createElement("div"));
	  if (!definePropertyWorksOnObject || !definePropertyWorksOnDom) {
	    var definePropertyFallback = defProp, definePropertiesFallback = defProps;
	  }
	}
	
	
	function defineProperty(object, property, descriptor) {
	  if ((typeof object !== "object" && typeof object !== "function") || object === null) {
	    throw new TypeError(ERR_NON_OBJECT_TARGET + object);
	  }
	  if ((typeof descriptor !== "object" && typeof descriptor !== "function") || descriptor === null) {
	    throw new TypeError(ERR_NON_OBJECT_DESCRIPTOR + descriptor);
	  }
	  // make a valiant attempt to use the real defineProperty
	  // for I8's DOM elements.
	  if (definePropertyFallback) {
	    try {
	      return definePropertyFallback.call(Object, object, property, descriptor);
	    } catch (exception) {
	      // try the shim if the real one doesn't work
	    }
	  }
	
	  // If it's a data property.
	  if (has(descriptor, "value")) {
	    // fail silently if "writable", "enumerable", or "configurable"
	    // are requested but not supported
	    if (supportsAccessors && (lookupGetter(object, property) ||
	                              lookupSetter(object, property)))
	      {
	        // As accessors are supported only on engines implementing
	        // `__proto__` we can safely override `__proto__` while defining
	        // a property to make sure that we don't hit an inherited
	        // accessor.
	        var prototype = object.__proto__;
	        object.__proto__ = prototypeOfObject;
	        // Deleting a property anyway since getter / setter may be
	        // defined on object itself.
	        delete object[property];
	        object[property] = descriptor.value;
	        // Setting original `__proto__` back now.
	        object.__proto__ = prototype;
	      } else {
	        object[property] = descriptor.value;
	      }
	  } else {
	    if (!supportsAccessors) {
	      throw new TypeError(ERR_ACCESSORS_NOT_SUPPORTED);
	    }
	    // If we got that far then getters and setters can be defined !!
	    if (has(descriptor, "get")) {
	      defineGetter(object, property, descriptor.get);
	    }
	    if (has(descriptor, "set")) {
	      defineSetter(object, property, descriptor.set);
	    }
	  }
	  return object;
	}
	
	
	function defineProperties(object, properties) {
	  // make a valiant attempt to use the real defineProperties
	  if (definePropertiesFallback) {
	    try {
	      return definePropertiesFallback.call(Object, object, properties);
	    } catch (exception) {
	      // try the shim if the real one doesn't work
	    }
	  }
	  for (var property in properties) {
	    if (has(properties, property) && property !== "__proto__") {
	      defineProperty(object, property, properties[property]);
	    }
	  }
	  return object;
	}
	
	
	if (!defProp || definePropertyFallback) {
	  module.exports = {
	    defineProperty: defineProperty,
	    defineProperties: defineProperties
	  };
	} else {
	  module.exports = __webpack_require__(12);
	}


/***/ },
/* 10 */
/***/ function(module, exports) {

	var hasOwn = Object.prototype.hasOwnProperty;
	
	
	module.exports = function has(obj, property) {
	  return hasOwn.call(obj, property);
	};


/***/ },
/* 11 */
/***/ function(module, exports) {

	var ERROR_MESSAGE = "Function.prototype.bind called on incompatible "
	var slice = Array.prototype.slice
	
	module.exports = bind
	
	function bind(that) {
	    var target = this
	    if (typeof target !== "function") {
	        throw new TypeError(ERROR_MESSAGE + target)
	    }
	    var args = slice.call(arguments, 1)
	
	    return function bound() {
	        if (this instanceof bound) {
	            var F = function () {}
	            F.prototype = target.prototype
	            var self = new F()
	
	            var result = target.apply(
	                self,
	                args.concat(slice.call(arguments))
	            )
	            if (Object(result) === result) {
	                return result
	            }
	            return self
	        } else {
	            return target.apply(
	                that,
	                args.concat(slice.call(arguments))
	            )
	        }
	    }
	}


/***/ },
/* 12 */
/***/ function(module, exports) {

	var defProp = Object.defineProperty, defProps = Object.defineProperties;
	
	
	module.exports = {
	  defineProperty: function defineProperty(object, property, descriptor) {
	    defProp.call(Object, object, property, descriptor);
	  },
	  defineProperties: function defineProperties(object, properties) {
	    defProps.call(Object, object, properties);
	  }
	};


/***/ },
/* 13 */
/***/ function(module, exports, __webpack_require__) {

	"use strict";
	
	var _bind = __webpack_require__(14);
	
	var _bind2 = _interopRequireDefault(_bind);
	
	var _create = __webpack_require__(17);
	
	var _create2 = _interopRequireDefault(_create);
	
	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }
	
	module.exports = (0, _bind2.default)(_create2.default, null, null);

/***/ },
/* 14 */
/***/ function(module, exports, __webpack_require__) {

	"use strict";
	
	var _functionBind = __webpack_require__(15);
	
	var _functionBind2 = _interopRequireDefault(_functionBind);
	
	var _uncurry = __webpack_require__(7);
	
	var _uncurry2 = _interopRequireDefault(_uncurry);
	
	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }
	
	module.exports = (0, _uncurry2.default)(_functionBind2.default);

/***/ },
/* 15 */
/***/ function(module, exports, __webpack_require__) {

	var implementation = __webpack_require__(16);
	
	module.exports = Function.prototype.bind || implementation;


/***/ },
/* 16 */
/***/ function(module, exports) {

	var ERROR_MESSAGE = 'Function.prototype.bind called on incompatible ';
	var slice = Array.prototype.slice;
	var toStr = Object.prototype.toString;
	var funcType = '[object Function]';
	
	module.exports = function bind(that) {
	    var target = this;
	    if (typeof target !== 'function' || toStr.call(target) !== funcType) {
	        throw new TypeError(ERROR_MESSAGE + target);
	    }
	    var args = slice.call(arguments, 1);
	
	    var bound;
	    var binder = function () {
	        if (this instanceof bound) {
	            var result = target.apply(
	                this,
	                args.concat(slice.call(arguments))
	            );
	            if (Object(result) === result) {
	                return result;
	            }
	            return this;
	        } else {
	            return target.apply(
	                that,
	                args.concat(slice.call(arguments))
	            );
	        }
	    };
	
	    var boundLength = Math.max(0, target.length - args.length);
	    var boundArgs = [];
	    for (var i = 0; i < boundLength; i++) {
	        boundArgs.push('$' + i);
	    }
	
	    bound = Function('binder', 'return function (' + boundArgs.join(',') + '){ return binder.apply(this,arguments); }')(binder);
	
	    if (target.prototype) {
	        var Empty = function Empty() {};
	        Empty.prototype = target.prototype;
	        bound.prototype = new Empty();
	        Empty.prototype = null;
	    }
	
	    return bound;
	};


/***/ },
/* 17 */
/***/ function(module, exports) {

	"use strict";
	
	module.exports = function () {
	  return Object.create || function (proto) {
	    function Type() {}
	    Type.prototype = proto;
	    return new Type();
	  };
	}();

/***/ },
/* 18 */
/***/ function(module, exports) {

	var hasOwn = Object.prototype.hasOwnProperty;
	
	module.exports = function (xs, f, acc) {
	    var hasAcc = arguments.length >= 3;
	    if (hasAcc && xs.reduce) return xs.reduce(f, acc);
	    if (xs.reduce) return xs.reduce(f);
	    
	    for (var i = 0; i < xs.length; i++) {
	        if (!hasOwn.call(xs, i)) continue;
	        if (!hasAcc) {
	            acc = xs[i];
	            hasAcc = true;
	            continue;
	        }
	        acc = f(acc, xs[i], i);
	    }
	    return acc;
	};


/***/ },
/* 19 */
/***/ function(module, exports, __webpack_require__) {

	'use strict';
	
	var _arrayReduce = __webpack_require__(18);
	
	var _arrayReduce2 = _interopRequireDefault(_arrayReduce);
	
	var _own = __webpack_require__(5);
	
	var _own2 = _interopRequireDefault(_own);
	
	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }
	
	module.exports = function (o, cb, start, thisArg) {
	  return (0, _arrayReduce2.default)((0, _own2.default)(o), function (r, v) {
	    return cb.call(thisArg, r, v, o);
	  }, start);
	};

/***/ },
/* 20 */
/***/ function(module, exports) {

	module.exports = isFunction
	
	var toString = Object.prototype.toString
	
	function isFunction (fn) {
	  var string = toString.call(fn)
	  return string === '[object Function]' ||
	    (typeof fn === 'function' && string !== '[object RegExp]') ||
	    (typeof window !== 'undefined' &&
	     // IE8 and below
	     (fn === window.setTimeout ||
	      fn === window.alert ||
	      fn === window.confirm ||
	      fn === window.prompt))
	};


/***/ },
/* 21 */
/***/ function(module, exports) {

	"use strict";
	
	module.exports = {
	  getter: function getter(prop) {
	    return 'get' + prop.substr(0, 1).toUpperCase() + prop.substr(1);
	  },
	  setter: function setter(prop) {
	    return 'set' + prop.substr(0, 1).toUpperCase() + prop.substr(1);
	  }
	};

/***/ }
/******/ ])
});
;
//# sourceMappingURL=singular.js.map