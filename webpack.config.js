"use strict";

var join = require('path').join,
  webpack = require('webpack'),
  deepAssign = require('deep-assign'),
  ClosureCompilerPlugin = require('webpack-closure-compiler'),
  assetPath = join(__dirname, 'src'),
  appendToSrc = join.bind(null, assetPath);

var base = {
  entry: appendToSrc('singular.js'),
  output: {
    path: __dirname,
    library: 'singular',
    libraryTarget: 'umd'
  },
  devtool: 'source-map',
  module: {
    loaders: [{
      test: /(?:\.js$)/,
      loader: 'babel-loader',
      exclude: /node_modules/,
      query: {
        presets: ['es2015', 'stage-2']
      }
    }]
  }
};
function fromBase(cfg) {
  return deepAssign(cfg, base);
}

module.exports = {
  dev: fromBase({
    output: {
      filename: 'singular.js'
    }
  }),
  min: fromBase({
    output: {
      filename: 'singular.min.js'
    },
    plugins: [
      new ClosureCompilerPlugin({
        compiler: {
          language_in: 'ECMASCRIPT6',
          language_out: 'ECMASCRIPT5'
        },
        concurrency: 3
      })
    ]
  })
};
